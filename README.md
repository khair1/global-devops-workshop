# Workshop Details
Partners: 
- Gitlab - CI/CD 
    -  taskcat to validates each cfn commit
- AWS Cloudformation - Deploy
    - deploys Vault via CFN
- HashiCorp Vault  - Secure
    - Secret Store
- Gremlin - Chaos Engineering
    - Fail a Vault Node


[Go to Labs](docs/workshop/devops/index.md)
